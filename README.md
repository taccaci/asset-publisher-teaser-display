# Asset Publisher Teaser Display

This hooks Asset Publisher to add a new display style called "Teaser". This
display looks up the templates available for the current [JournalArticle][0] and
[JournalStructure][1]. It then filters the templates list for a template that
matches `/.*teaser.*/i`, i.e. the template has the word "Teaser" in its name. If
a Teaser template is located the content is rendered with that template.
Otherwise, this display renders the same as the "Abstracts" display style.

[0]: http://cdn.docs.liferay.com/portal/6.1/javadocs/com/liferay/portlet/journal/model/JournalArticle.html
[1]: http://cdn.docs.liferay.com/portal/6.1/javadocs/com/liferay/portlet/journal/model/JournalStructure.html
